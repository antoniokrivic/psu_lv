import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(open("mtcars.csv", "rb"), usecols=(1,2,3,4,5,6), delimiter=",", skiprows=1)

cyl= data[:,2]

print("Max potrosnja: ", min(cyl))
print("Min potrosnja: ", max(cyl))
print("Mean cyl", np.mean(cyl))
plt.scatter(cyl)
plt.xlabel('cyl')

plt.show()




