try:
    ocjena=float(input("Unesi ocjenu izmedu 0.0 i 1.0: "))
except:
    print("Nije upisan broj")
    exit()

if ocjena<0.0:
    print("Broj nije odgovarajuci")
elif ocjena>1.0:
    print("Broj nije odgovrajuci")
elif ocjena>=0.9:
    print("A")
elif ocjena>=0.8:
    print("B")
elif ocjena>=0.7:
    print("C")
elif ocjena>=0.6:
    print("D")
elif ocjena<0.6:
    print("F")  